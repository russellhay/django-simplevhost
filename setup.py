from setuptools import setup

setup(
    name='django-simplevhost',
    version='0.2',
    packages=['simplevhost', 'simplevhost.migrations', 'simplevhost.templatetags'],
    url='https://bitbucket.org/russellhay/django-simplevhost',
    author='Russell Hay',
    author_email='me@russellhay.com',
)
